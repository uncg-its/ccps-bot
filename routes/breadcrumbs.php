<?php

// HOME
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push("Home", route('home'));
});

// ACCOUNT
Breadcrumbs::register('account', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("Account", route('account'));
});
Breadcrumbs::register('profile.show', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push("My Profile", route('profile.show'));
});
Breadcrumbs::register('profile.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push("Edit Profile", route('profile.edit'));
});
Breadcrumbs::register('account.settings', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push("Account Settings", route('account.settings'));
});

// Notifications
Breadcrumbs::register('notifications.index', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push('Notifications', route('account.notifications.index'));
});
Breadcrumbs::register('notifications.channels.create', function ($breadcrumbs) {
    $breadcrumbs->parent('notifications.index');
    $breadcrumbs->push('Add New Channel', route('account.notifications.channels.create'));
});
Breadcrumbs::register('notifications.configure', function ($breadcrumbs) {
    $breadcrumbs->parent('notifications.index');
    $breadcrumbs->push('Configure', route('account.notifications.configure'));
});

// API Tokens
Breadcrumbs::register('tokens.index', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push('API Tokens', route('account.tokens.index'));
});
Breadcrumbs::register('tokens.create', function ($breadcrumbs) {
    $breadcrumbs->parent('tokens.index');
    $breadcrumbs->push('Create API Token', route('account.tokens.create'));
});
Breadcrumbs::register('tokens.edit', function ($breadcrumbs, $token) {
    $breadcrumbs->parent('tokens.index');
    $breadcrumbs->push('Edit API Token ' . $token->id, route('account.tokens.edit', $token));
});


// ADMIN
Breadcrumbs::register('admin', function ($breadcrumbs) {
    $breadcrumbs->push("Admin", route('admin'));
});

// Package breadcrumbs
foreach (config('ccps.modules') as $name => $module) {
    require(base_path('vendor/' . $module['package'] . '/src/breadcrumbs/' . $name . '.php'));
}
