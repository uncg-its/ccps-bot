<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\BotManController;

Route::get('/', [\App\Http\Controllers\CcpsCore\HomeController::class, 'index'])->name('home');
Route::get('home', [\App\Http\Controllers\CcpsCore\HomeController::class, 'index']);

Route::group(['prefix' => 'account'], function () {
    Route::get('/', [\App\Http\Controllers\CcpsCore\AccountController::class, 'index'])->name('account');
    Route::get('/settings', [\App\Http\Controllers\CcpsCore\AccountController::class, 'settings'])->name('account.settings');
    Route::post('/settings', [\App\Http\Controllers\CcpsCore\AccountController::class, 'updateSettings'])->name('account.settings.update');
    Route::get('/profile', [\App\Http\Controllers\CcpsCore\AccountController::class, 'profile'])->name('profile.show');
    Route::get('/profile/edit', [\App\Http\Controllers\CcpsCore\AccountController::class, 'editProfile'])->name('profile.edit');
    Route::patch('/profile/', [\App\Http\Controllers\CcpsCore\AccountController::class, 'updateProfile'])->name('profile.update');

    Route::prefix('tokens')->name('account.tokens.')->group(function () {
        Route::get('/', [\App\Http\Controllers\CcpsCore\AccountController::class, 'tokens'])->name('index');
    });

    Route::group(['prefix' => 'notifications'], function () {
        Route::get('/', [\App\Http\Controllers\CcpsCore\AccountController::class, 'notifications'])->name('account.notifications.index');
    });
});

// CCPS Core routes
Uncgits\Ccps\Support\Facades\CcpsCore::routes();

// For local auth
Auth::routes();

// Socialite - User Account OAuth Routes
Route::get('auth/{provider}', [\App\Http\Controllers\CcpsCore\AuthController::class, 'redirectToProvider'])->name('oauth');
Route::get('auth/{provider}/callback', [\App\Http\Controllers\CcpsCore\AuthController::class, 'handleProviderCallback'])->name('oauth.callback');

// Bot routes

$botman = config('ccps-bot.url_suffix');

Route::match(['get', 'post'], '/' . $botman, [BotManController::class, 'handle'])->name('botman');
Route::get('/' . $botman . '/tinker', [BotManController::class, 'tinker'])->name('tinker');

//

Route::middleware(['auth'])->group(function () {
    Route::get('cd', \App\Http\Livewire\CommandDashboard::class);
});
