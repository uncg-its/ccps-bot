<?php

namespace App\Conversations;

use App\WstApiHelper;
use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;

class WebexConversation extends Conversation
{
    protected $wstApi;

    protected $loadedResponse;

    protected $username;

    public function __construct($loadedResponse = null, $username = null)
    {
        $this->wstApi = new WstApiHelper();
        $this->loadedResponse = $loadedResponse;
        $this->username = $username;
    }

    /**
     * First question
     */
    public function askFunction()
    {
        // $question = Question::create("All right, Webex. What do you need?")
        //     ->fallback('Unable to ask question')
        //     ->callbackId('ask_reason')
        //     ->addButtons([
        //         Button::create('Recording Usage')->value('recording-usage'),
        //         Button::create('User Eligibility')->value('user-eligibility'),
        //     ]);

        if ($this->loadedResponse == 'rec') {
            return $this->getRecordingUsage();
        } elseif ($this->loadedResponse == 'user') {
            if (!is_null($this->username)) {
                return $this->getUserEligibility($this->username);
            } else {
                return $this->askUsername();
            }
        }

        $this->ask("All right, Webex. What do you need? Say 'rec' for Recording Usage, or 'user' for User Eligibility, or 'back' to go back.",
            function (Answer $answer) {
                try {
                    $ansText = $answer->getText();
                    // $this->say('you answered ' . $ansText);
                    if ($ansText === 'rec') {
                        $this->getRecordingUsage();
                    } elseif ($ansText === 'user') {
                        $this->askUsername();
                    } elseif ($ansText === 'back') {
                        $this->say('OK, taking you back.');
                        return;
                    } else {
                        $this->say("Sorry, that wasn't an option. Try again.");
                    }
                } catch (\Exception $e) {
                    $this->say('Whoops. ' . $e->getMessage());
                }
            });
    }

    public function askUsername()
    {
        $this->ask("What is the username you want to look up?", function (Answer $answer) {
            $username = $answer->getText();
            $this->getUserEligibility($username);
        });
    }

    /**
     * Start the conversation
     */
    public function run()
    {
        $this->askFunction();
    }

    private function getRecordingUsage()
    {
        $usage = $this->wstApi->lookupWstRecordingUsage();

        $this->say('WST reports Current Webex Recording usage at ' . $usage . ' MB');
    }

    private function getUserEligibility($username)
    {
        // $this->say('Fetching eligibility for user ' . $this->username);
        $data = $this->wstApi->lookupWstUserEligibility($username);

        // found in source?
        if (!$data->wasFoundInSource) {
            $this->say('Hmm, user ' . $username . ' was not found in the user source data.');
        } else {
            // eligible / active?
            if ($data->isEligible) {
                $this->say('User ' . $username . ' is eligible for Webex. Reason: ' . $data->qualifiesVia);
                // active?
                if ($data->isActive) {
                    $this->say('The user is currently active in Webex.');
                } else {
                    $this->say('The user is not yet active in Webex.');
                }
            } else {
                $this->say('User ' . $username . ' is not eligible for Webex.');
            }
        }
    }
}
