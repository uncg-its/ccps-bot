<?php

namespace App;

use BotMan\BotMan\BotMan;
use Illuminate\Database\Eloquent\Model;

class Command extends Model
{
    protected $fillable = [
        'driver',
        'user_name',
        'email',
        'user_id',
        'message',
        'service',
        'core_command',
        'understood',
    ];

    // service-like stuff

    public static function logFromBot(BotMan $bot, $understood = true)
    {
        $email = '';
        if (isset($bot->getUser()->getInfo()['email'])) {
            $email = $bot->getUser()->getInfo()['email'];
        } elseif (\Auth::user()) {
            $email = \Auth::user()->email;
        }

        return self::create([
            'driver'       => $bot->getDriver()->getName(),
            'user_name'    => $bot->getUser()->getUsername(),
            'email'        => $email,
            'user_id'      => $bot->getUser()->getId(),
            'message'      => $bot->getMessage()->getText(),
            'service'      => $bot->service ?? null,
            'core_command' => $bot->core_command ?? null,
            'understood'   => $understood,
        ]);
    }

    public function scopeRecent($query, $number)
    {
        $query->latest()->take($number);
    }

    public function scopeUnderstood($query)
    {
        $query->where('understood', true);
    }

    public function scopeNotUnderstood($query)
    {
        $query->where('understood', false);
    }

    public function scopeFromUser($query, $email)
    {
        $query->where('email', $email);
    }

    public function scopeService($query, $service)
    {
        $query->where('service', $service);
    }
}
