<?php

namespace App\Console;

use App\CcpsCore\CronjobMeta;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        try {
            $result = \DB::connection()->getPdo();
            if (\Schema::hasTable('ccps_cronjob_meta')) {
                $cronjobsToRun = CronjobMeta::where('status', 'enabled')->get();

                $cronjobsToRun->each(function ($cronjobMeta) use ($schedule) {
                    $cronClass = new $cronjobMeta->class;
                    $schedule->call(\Closure::fromCallable([$cronClass, 'run']))
                    ->cron($cronClass->getSchedule())
                    ->name(\Str::studly($cronClass->getClassName()))
                    ->withoutOverlapping();
                });
            } else {
                \Log::channel('cron')->warning('Tried to load cronjob meta, but could not find the ccps_cronjob_meta table!', [
                    'category'  => 'cron',
                    'operation' => 'register',
                    'result'    => 'failure',
                    'data'      => []
                ]);
            }
        } catch (\PDOException $e) {
            \Log::channel('cron')->info('Could not connect to database; skipping schedule load', [
                'category'  => 'cron',
                'operation' => 'register',
                'result'    => 'failure',
                'data'      => []
            ]);
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
