<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Finder\Finder;

class GenerateNewBotmanUrl extends Command
{
    const MAX_LENGTH = 50;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'botman:url
                            {--length=24 : length of the new url suffix (max ' . self::MAX_LENGTH . ')}
                            {--display=n : output to the screen instead of subbing into the .env file directly}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a new URL suffix for Botman for the .env file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $length = (int)$this->option('length');

        if (!is_int($length)) {
            $this->error('Error: length must be an integer.');
            return;
        }

        if ($length > self::MAX_LENGTH) {
            $this->error('Error: length cannot be greater than ' . self::MAX_LENGTH . '.');
        }

        $adjustLength = false;
        if ($length % 2 != 0) {
            $adjustLength = true;
            $length++;
        }

        $suffix = bin2hex(random_bytes($length / 2));

        if ($adjustLength) {
            $suffix = substr($suffix, 0, strlen($suffix) - 1);
        }

        $this->info('New suffix created: ' . $suffix);

        if ($this->option('display') == 'y') {
            return;
        }

        $finder = new Finder();
        $finder->files()->ignoreDotFiles(false)->in(base_path())->name('.env');

        foreach ($finder as $file) {
            $envContents = $file->getContents();

            $new = 'BOTMAN_URL_SUFFIX=' . $suffix;

            $botmanSuffixEnvEntry = preg_replace('/BOTMAN_URL_SUFFIX=[0-9a-zA-Z]+/', $new, $envContents, 1, $count);

            if ($count == 0) {
                $botmanSuffixEnvEntry .= PHP_EOL . $new;// add it
            }

            $write = file_put_contents($file->getPathname(), $botmanSuffixEnvEntry);
            $this->info('Suffix written to .env file');
            return;
        };

    }
}
