<?php

namespace App\Helpers;

use App\Exceptions\CcpsBotException;
use App\Exceptions\NotFoundException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class SimpleHttp
{
    public static function getRequest($client, $url)
    {
        try {
            $result = $client->get($url);
            return $result;
        } catch (ClientException $e) {
            // 400-level
            if ($e->getResponse()->getStatusCode() === 404) {
                throw new NotFoundException('Resource not found');
            }

            throw new CcpsBotException($e->getResponse()->getStatusCode() . ' Error: ' . $e->getResponse()->getReasonPhrase());
            // $bot->reply($e->getResponse()->getStatusCode() . ' Error: ' . $e->getResponse()->getReasonPhrase());
        } catch (ServerException $e) {
            // 500-level
            // $bot->reply($e->getResponse()->getStatusCode() . ' Error: ' . $e->getResponse()->getReasonPhrase());
            throw new CcpsBotException($e->getResponse()->getStatusCode() . ' Error: ' . $e->getResponse()->getReasonPhrase());
        } catch (\Throwable $th) {
            // $bot->reply('Bot error: ' . $th->getMessage());
            throw new CcpsBotException($th->getMessage());
        }
    }
}
