<?php

namespace App\Helpers;

use GuzzleHttp\Client;

class Gst extends SimpleHttp
{
    public function get($url)
    {
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . config('services.ccps-gst.key'),
                'Content-type'  => 'application/json',
                'Accept'        => 'application/json'
            ]
        ]);

        return $this->getRequest($client, $url);
    }
}
