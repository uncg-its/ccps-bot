<?php

namespace App\Helpers;

use GuzzleHttp\Client;

class UserLookup extends SimpleHttp
{
    public function get($url)
    {
        $client = new Client([
            'headers' => [
                'Authorization' => 'Bearer ' . config('services.ccps-user-lookup.key'),
                'Content-type'  => 'application/json',
                'Accept'        => 'application/json'
            ]
        ]);

        return $this->getRequest($client, $url);
    }

    public function getEndpoint()
    {
        return config('services.ccps-user-lookup.endpoint');
    }
}
