<?php

namespace App\BotCommands;

use Carbon\Carbon;
use App\Helpers\Cst;

class Canvas extends Base
{
    protected $helper;
    protected $service = 'canvas';

    public function __construct()
    {
        $this->helper = new Cst;
    }

    public function help($bot)
    {
        return $this->handle($bot, function () use ($bot) {
            $bot->reply("Canvas commands:\n\n*canvas user {username}* - looks up a user's basic information\n*canvas user {username} quota* - shows how much of a user's personal file quota has been used\n*canvas courses {username}* - shows current-term courses for a user\n*canvas courses {username} term {term}* - shows courses for a user in a specified term (MMMMYY format)\n*canvas findcourse {search}* - lists courses from the current term that match a search query - useful to find the ID of a course and get its details\n*canvas findcourse {search} term {term}* - lists courses from the given term that match a search query (MMMMYY format)\n*canvas course {canvas_id}* - shows a canvas course details\n*canvas course {canvas_id} enrollments* - shows enrollments in a canvas course (chunked)\n*canvas course {canvas_id} quota* - shows how much of the course quota has been used\n\n_Note: use double quotes if searching for something that has spaces, like \"BIO 101\"_");
        });
    }

    public function searchCourses($bot, $prefix, $query, $term = null)
    {
        $query = str_replace('"', '', $query);

        if (strlen($query) <= 3) {
            return $this->handle($bot, function () use ($bot) {
                $bot->reply('Sorry, your search needs to be at least 3 characters long.');
            });
        }

        $suffix = is_null($term) ? '' : '&term=' . $term;

        return $this->handle($bot, function () use ($bot, $query, $term, $suffix) {
            $url = $this->helper->getEndpoint() . '/courses/search?query=' . $query . $suffix;
            $result = $this->helper->get($url)->getBody()->getContents();

            $courses = json_decode($result)->courses;
            $term = json_decode($result)->term;

            if (count($courses) === 0) {
                $bot->reply("I couldn't find any courses matching *$query* in term *$term*.");
            } else {
                $courseInfo = "";
                foreach ($courses as $course) {
                    $courseInfo .= "- [{$course->course_code}] {$course->name} ({$course->workflow_state}) - _Canvas ID {$course->id}_\n";
                }
                $bot->reply("Search results for term *$term*:\n" . $courseInfo);
            }
        });
    }

    public function searchUser($bot, $prefix, $username)
    {
        return $this->handle($bot, function () use ($bot, $username) {
            $url = config('services.ccps-cst.endpoint') . '/users/' . $username;
            $result = $this->helper->get($url)->getBody()->getContents();

            $user = json_decode($result)->user;

            $response = "User *$username:*\n- Canvas ID: {$user->id}\n- Name: {$user->name}\n- Created at: {$user->created_at}";
            $bot->reply($response);
        }, "Sorry, I couldn't find a user with that username!");
    }

    public function listUserCourses($bot, $prefix, $username, $term = null)
    {
        $suffix = is_null($term) ? '' : '?term=' . $term;

        return $this->handle($bot, function () use ($bot, $username, $term, $suffix) {
            $url = config('services.ccps-cst.endpoint') . '/users/' . $username . '/courses' . $suffix;
            $result = $this->helper->get($url)->getBody()->getContents();

            $courses = json_decode($result)->courses;
            $term = json_decode($result)->term;

            if (count($courses) === 0) {
                $bot->reply("User *$username* has no courses term *$term*.");
            } else {
                $courseInfo = "";
                foreach ($courses as $course) {
                    $courseInfo .= "- [{$course->course_code}] {$course->name} ({$course->workflow_state}) - _Canvas ID {$course->id}_\n";
                }
                $bot->reply("User *$username* is enrolled in the following courses for term *$term*:\n" . $courseInfo);
            }
        }, "Sorry, I couldn't find a user with that username!");
    }

    public function listCourseEnrollments($bot, $id)
    {
        return $this->handle($bot, function () use ($bot, $id) {
            $url = config('services.ccps-cst.endpoint') . '/courses/' . $id . '/enrollments';
            $result = $this->helper->get($url)->getBody()->getContents();

            $enrollments = json_decode($result)->enrollments;

            if (count($enrollments) === 0) {
                $bot->reply("Course *$id* has no enrollments.");
            } else {
                // Google Chat driver can't reply more than once, so this is necessary.
                $enrollmentChunks = collect($enrollments)->chunk(50);

                $bot->userStorage()->save([
                    'chunks' => [
                        'id'            => $id,
                        'type'          => 'enrollment',
                        'chunks'        => $enrollmentChunks,
                        'previousChunk' => 0,
                        'lastChunk'     => $enrollmentChunks->count() - 1
                    ]
                ]);

                $this->showEnrollmentChunk(0, $bot);
            }
        }, "Sorry, I couldn't find a course with that ID!");
    }

    public function showCourse($bot, $prefix, $id)
    {
        return $this->handle($bot, function () use ($bot, $id) {
            $url = config('services.ccps-cst.endpoint') . '/courses/' . $id;
            $result = $this->helper->get($url)->getBody()->getContents();

            $course = json_decode($result)->course;
            $teachers = collect($course->teachers)->implode('display_name', ', ');
            $concluded = $course->concluded ? 'yes' : 'no';
            $start = $course->start_at ?? ($course->term->start_at ?? 'not set');
            if ($start !== 'not set') {
                $start = Carbon::parse($start)->setTimezone('America/New_York')->format('m/d/Y H:i:s T');
            }
            $end = $course->end_at ?? ($course->term->end_at ?? 'not set');
            if ($end !== 'not set') {
                $end = Carbon::parse($end)->setTimezone('America/New_York')->format('m/d/Y H:i:s T');
            }
            $restricted = $course->restrict_enrollments_to_course_dates ? 'yes' : 'no';

            $response = "Course *$id:*\n- Name: {$course->name}\n- Code: {$course->course_code}\n- Term: {$course->term->name}\n- Total Students: {$course->total_students}\n- Workflow state: {$course->workflow_state}\n- Teacher(s): {$teachers}\n- Start Date: {$start}\n- End Date: {$end}\n- Access Restricted to Course Dates?: {$restricted}\n- Concluded?: {$concluded}\n- Created at: {$course->created_at}";
            $bot->reply($response);
        }, "Sorry, couldn't find a course with that ID! Make sure you're using the Canvas ID, which is a number.");
    }

    public function showUserQuota($bot, $prefix, $username)
    {
        return $this->handle($bot, function () use ($bot, $username) {
            $url = config('services.ccps-cst.endpoint') . '/users/' . $username . '/quota';
            $result = $this->helper->get($url)->getBody()->getContents();

            $quota = json_decode($result)->quota_information;

            $used = round($quota->quota_used / 1024 / 1024, 2); // MB
            $allowed = round($quota->quota / 1024 / 1024, 2); // MB
            $percentUsed = round($used / $allowed, 4) * 100;

            $response = "User *$username:* $used MB out of $allowed MB ($percentUsed%) of their personal storage quota has been used\n";
            $bot->reply($response);
        }, "Sorry, I couldn't find a user with that username!");
    }

    public function showCourseQuota($bot, $id)
    {
        return $this->handle($bot, function () use ($bot, $id) {
            $url = config('services.ccps-cst.endpoint') . '/courses/' . $id . '/quota';
            $result = $this->helper->get($url)->getBody()->getContents();

            $quota = json_decode($result)->quota_information;

            $used = round($quota->quota_used / 1024 / 1024, 2); // MB
            $allowed = round($quota->quota / 1024 / 1024, 2); // MB
            $percentUsed = round($used / $allowed, 4) * 100;

            $response = "Course *$id:* $used MB out of $allowed MB ($percentUsed%) of the course storage quota has been used\n";
            $bot->reply($response);
        }, "Sorry, couldn't find a course with that ID! Make sure you're using the Canvas ID, which is a number.");
    }

    public function showEnrollmentChunk($index, $bot)
    {
        return $this->handle($bot, function () use ($index, $bot) {
            $chunks = $bot->userStorage()->get('chunks');
            if ($index > $chunks['lastChunk']) {
                $bot->reply('No more chunks to show.');
            } elseif ($chunks['type'] !== 'enrollment') {
                $bot->reply('Error: invalid enrollment chunks.');
            } else {
                $enrollmentInfo = "";
                foreach ($chunks['chunks'][$index] as $enrollment) {
                    $enrollment = json_decode(json_encode($enrollment));
                    $enrollmentInfo .= "- {$enrollment->user->sortable_name} ({$enrollment->user->login_id}): _{$enrollment->role}_\n";
                }
                $indexDisplay = $index + 1;
                $lastChunkDisplay = $chunks['lastChunk'] + 1;

                $bot->reply("Enrollments (chunk $indexDisplay of {$lastChunkDisplay}) for course *{$chunks['id']}*:\n" . $enrollmentInfo);

                $chunks['previousChunk'] = $index;
                $bot->userStorage()->save(['chunks' => $chunks]);
            }
        });
    }
}
