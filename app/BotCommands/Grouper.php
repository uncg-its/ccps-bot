<?php

namespace App\BotCommands;

use App\Exceptions\NotFoundException;
use App\Helpers\UserLookup;

class Grouper extends Base
{
    protected $helper;
    protected $service = 'grouper';

    public function __construct()
    {
        $this->helper = new UserLookup;
    }

    public function help($bot)
    {
        return $this->handle($bot, function () use ($bot) {
            $bot->reply("Grouper commands:\n\n*grouper user {username}* - looks up a user in Grouper\n");
        });
    }

    public function lookupUser($bot, $username)
    {
        $this->handle($bot, function () use ($bot, $username) {
            //user
            $url = config('services.ccps-user-lookup.endpoint') . '/grouper/lookup?terms=' . $username;
            $result = $this->helper->get($url)->getBody()->getContents();

            $found = json_decode($result, true)['results'];
            $notFound = json_decode($result, true)['not_found'];

            if (in_array($username, $notFound)) {
                throw new NotFoundException;
            }

            $response = "User *$username*:\n";
            foreach ($found[$username] as $property => $value) {
                $response .= "- _{$property}_: $value\n";
            }

            $bot->reply($response);
        }, "Sorry, I couldn't find a user with that username!");
    }
}
