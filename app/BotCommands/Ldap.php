<?php

namespace App\BotCommands;

use App\Exceptions\NotFoundException;
use App\Helpers\UserLookup;
use Carbon\Carbon;

class Ldap extends Base
{
    protected $helper;
    protected $service = 'ldap';

    public function __construct()
    {
        $this->helper = new UserLookup;
    }

    public function help($bot)
    {
        return $this->handle($bot, function () use ($bot) {
            $bot->reply("LDAP commands:\n\n*ldap user {username}* - looks up a user in LDAP\n");
        });
    }

    public function lookupUser($bot, $username)
    {
        $this->handle($bot, function () use ($bot, $username) {
            //user
            $url = config('services.ccps-user-lookup.endpoint') . '/ldap/lookup?terms=' . $username;
            $result = $this->helper->get($url)->getBody()->getContents();

            $decoded = json_decode($result, true)['results'];
            $mapped = collect($decoded)->keyBy(function ($value) {
                return $value['cn'][0];
            })->toArray();

            if (!isset($mapped[strtoupper($username)])) {
                throw new NotFoundException;
            }

            $response = "User *$username*:\n";
            foreach ($mapped[strtoupper($username)] as $property => $valueArray) {
                $response .= "- _{$property}_: " . implode(', ', $valueArray ?? []) . "\n";
            }

            $bot->reply($response);
        }, "Sorry, I couldn't find a user with that username!");
    }
}
