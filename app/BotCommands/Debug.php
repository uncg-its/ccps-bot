<?php

namespace App\BotCommands;

use App\Traits\ProtectsCommands;

class Debug extends Base
{
    use ProtectsCommands;

    public $service = 'debug';

    public function me($bot)
    {
        $this->handle($bot, function () use ($bot) {
            $bot->reply(json_encode($bot->getUser()->getInfo()));
        });
    }

    public function protected($bot)
    {
        return $this->handle($bot, function () use ($bot) {
            if ($this->userIsAdmin($bot)) {
                $bot->reply('Keep it secret. Keep it safe.');
            } else {
                $bot->reply('You are not authorized to perform this action.');
            }
        });
    }

    public function payload($bot)
    {
        return $this->handle($bot, function () use ($bot) {
            $bot->reply(($bot->getMessage()->getText()));
        });
    }

    public function driver($bot)
    {
        return $this->handle($bot, function () use ($bot) {
            $bot->reply($bot->getDriver()->getName());
        });
    }
}
