<?php

namespace App\BotCommands;

use App\Exceptions\NotFoundException;

abstract class Base
{
    public function handle($bot, $callback, $notFoundMessage = 'No results for that query.')
    {
        try {
            $bot->service = $this->service;
            $bot->core_command = debug_backtrace()[1]['function'];

            $result = call_user_func($callback);
        } catch (NotFoundException $e) {
            $bot->reply($notFoundMessage);
        } catch (\Throwable $th) {
            $bot->reply('Whoops, something went wrong with that request. Sorry!');
            report($th);
        }
    }
}
