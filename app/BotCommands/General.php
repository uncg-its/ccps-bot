<?php

namespace App\BotCommands;

class General extends Base
{
    protected $service = 'general';

    public function help($bot)
    {
        return $this->handle($bot, function () use ($bot) {
            $bot->reply("Sure thing! Right now I have commands available for *Google (Workspace)*, *Canvas*, *Zoom*, *LDAP*, and *Grouper*. Type \"help\" followed by the name of the service (example \"help google\") to learn more about them.\n\nFor results that are chunked, use \"next chunk\" or simply \"next\" to move to the next chunk of results.\n\nI hope to be smarter over time. Check back with me often!");
        });
    }

    public function hello($bot)
    {
        return $this->handle($bot, function () use ($bot) {
            $bot->reply('Well howdy there.');
        });
    }

    public function ccps($bot)
    {
        return $this->handle($bot, function () use ($bot) {
            $response = "```\n";
            $response .= "    _\n";
            $response .= "   /_\__      _____  ___  ___  _ __ ___   ___\n";
            $response .= "  //_\\\\ \ /\ / / _ \/ __|/ _ \| '_ ` _ \ / _ \ \n";
            $response .= " /  _  \ V  V /  __/\__ \ (_) | | | | | |  __/\n";
            $response .= " \_/ \_/\_/\_/ \___||___/\___/|_| |_| |_|\___|\n";
            $response .= "                                               \n";
            $response .= "   _ __   ___  ___ ___ _   _ _ __ ___\n";
            $response .= "  | '_ \ / _ \/ __/ __| | | | '_ ` _ \ \n";
            $response .= "  | |_) | (_) \__ \__ \ |_| | | | | | |\n";
            $response .= "  | .__/ \___/|___/___/\__,_|_| |_| |_|\n";
            $response .= "  |_|\n";
            $response .= "```";

            $bot->reply($response);
        });
    }

    public function fallback($bot)
    {
        // don't run this through handle() since it wasn't understood.
        $text = $bot->getMessage()->getText();
        $bot->reply('Sorry, I don\'t understand "' . $text . '". Type "help" to see what I can currently do');
    }
}
