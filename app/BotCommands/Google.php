<?php

namespace App\BotCommands;

use App\Helpers\Gst;

class Google extends Base
{
    protected $helper;
    protected $service = 'gsuite';

    public function __construct()
    {
        $this->helper = new Gst;
    }

    public function help($bot)
    {
        return $this->handle($bot, function () use ($bot) {
            $helpText = "Google Workspace (formerly G Suite) commands:\n\n";
            $helpText .= "*workspace/gsuite/google whatis {address}* - Is an address a user, alias or a group?\n";
            $helpText .= "*workspace/gsuite/google user {username} account [aliases|tokens]* - looks up a user's information\n";
            // $helpText .= "*gsuite aliases {username}* - shows aliases for a user\n";
            // $helpText .= "*workspace user {username} email* - user's email message count and information about configuration options that can be queried\n";
            $helpText .= "*workspace/gsuite/google user {username} email [delegates|filters|forwards|labels|imap|pop|sendas|vacation]* - user's email configuration\n";
            $helpText .= "*workspace/gsuite/google user {username} groups* - shows groups for a user\n";

            // $helpText .= "*gsuite forwards {username}* - shows email forwards for a user\n";
            // $helpText .= "*gsuite filters {username}* - shows email filters for a user";
            $helpText .= "*workspace/gsuite/google group {email} info [membership|settings]* - shows info for a group\n";
            $helpText .= "*workspace/gsuite/google group {email} member {address}* - shows the info of a group member (user or group)";

            $bot->reply($helpText);
            //$bot->reply("G Suite commands:\n\n*gsuite whatis {address}* - Is an address a user, alias or a group?\n*gsuite user {username}* - looks up a user's basic information\n*gsuite aliases {username}* - shows aliases for a user\n*gsuite tokens {username}* - shows active tokens on a user's account\n*gsuite groups {username}* - shows groups for a user\n*gsuite forwards {username}* - shows email forwards for a user\n*gsuite filters {username}* - shows email filters for a user");
        });
    }

    /*
      Docuemntation for Group Settings
      https://developers.google.com/admin-sdk/groups-settings/v1/reference/groups#json
    */

    public function showGroup($bot, $email, $config = null)
    {
        return $this->handle($bot, function () use ($bot, $email, $config) {
            $url = config('services.ccps-gst.endpoint') . '/groups/' . $email . '/' . $config;
            $result = $this->helper->get($url)->getBody()->getContents();
            if ($config == "membership") {
                $members = json_decode($result)->members->groupMembers;

                if (count($members) === 0) {
                    $bot->reply("I couldn't find any members for group  *$email*.");
                } else {
                    $response = "Members for group  *$email*\n";
                    $memberInfo = "";
                    foreach ($members as $member) {
                        $memberInfo .= "- {$member->email} ({$member->type}): $member->role \n";
                    }
                    // $bot->reply("Search results for term *$term*:\n" . $courseInfo);
                    $response .= $memberInfo;
                }
                $bot->reply($response);
            } elseif ($config == "settings") {
                $group = json_decode($result)->group;

                $response = "Group *$email:*\n";
                $response .= "- Name: {$group->name}\n";
                $response .= "- Description: {$group->description}\n";
                $response .= "- Is Collaborative Inbox? {$group->enableCollaborativeInbox}\n";
                $response .= "- Can have external (non-UNCG) members?: {$group->allowExternalMembers}\n";

                /*
                Options for whoCanJoin
                ANYONE_CAN_JOIN: Any Internet user, both inside and outside your domain, can join the group.
                ALL_IN_DOMAIN_CAN_JOIN: Anyone in the account domain can join. This includes accounts with multiple domains.
                INVITED_CAN_JOIN: Candidates for membership can be invited to join.
                CAN_REQUEST_TO_JOIN: Non members can request an invitation to join.
                */
                if ($group->whoCanJoin == "ANYONE_CAN_JOIN") {
                    $whoCanJoin = "Any Internet user, both inside and outside the domain, can join the group.";
                } elseif ($group->whoCanJoin == "ALL_IN_DOMAIN_CAN_JOIN") {
                    $whoCanJoin = "Any user in the domain can join.";
                } elseif ($group->whoCanJoin == "INVITED_CAN_JOIN") {
                    $whoCanJoin = "Those who are invited. Candidates for membership can be invited to join.";
                } elseif ($group->whoCanJoin == "CAN_REQUEST_TO_JOIN") {
                    $whoCanJoin = "Those who request membership. Non members can request an invitation to join.";
                } else {
                    $whoCanJoin = "Unknown. Sorry. An error occurred";
                }
                $response .= "- Who can join this group?: $whoCanJoin\n";


                /*
                Options for whoCanPostMessages
                ALL_MANAGERS_CAN_POST: Managers, including group owners, can post messages.
                ALL_MEMBERS_CAN_POST: Any group member can post a message.
                ALL_OWNERS_CAN_POST: Only group owners can post a message.
                ALL_IN_DOMAIN_CAN_POST: Anyone in the account can post a message.
                ANYONE_CAN_POST: Any Internet user who outside your account can access your Google Groups service and post a message.
                */

                if ($group->whoCanPostMessage == "ALL_MANAGERS_CAN_POST") {
                    $whoCanPost = "Managers, including group owners, can post messages.";
                } elseif ($group->whoCanPostMessage == "ALL_MEMBERS_CAN_POST") {
                    $whoCanPost = "Any group member can post a message.";
                } elseif ($group->whoCanPostMessage == "ALL_OWNERS_CAN_POST") {
                    $whoCanPost = "Only group owners can post a message.";
                } elseif ($group->whoCanPostMessage == "ALL_IN_DOMAIN_CAN_POST") {
                    $whoCanPost = "Anyone in the account can post a message.";
                } elseif ($group->whoCanPostMessage == "ANYONE_CAN_POST") {
                    $whoCanPost = "Any Internet user who outside your account can access your Google Groups service and post a message.";
                } else {
                    $whoCanPost = "Unknown. Sorry. An error occurred";
                }
                $response .= "- Who can post messages?: $whoCanPost\n";
           

                if ($group->isArchived == "TRUE") {
                    $rchived = "Yes";
                } else {
                    $archived = "No";
                }
                $response .= "- Messages archived?: $archived\n";


                //  $response .= "- Description: {$group->description}\n";
                //  $response .= "- Description: {$group->description}\n";
                //  $response .= "- Description: {$group->description}\n";
                //  $response .= "- Description: {$group->description}\n";
                //  $response .= "- Description: {$group->description}\n";
                //  $response .= "- Description: {$group->description}\n";


                $bot->reply($response);
            } else {
                $info = json_decode($result)->group;
                $response = "Group *$email*\n";
                $response .= "Name: " . $info->name ."\n";
                $response .= "Description: " . $info->description ."\n";
                $response .= "Members: " . $info->directMembersCount ." members.\n";
                $response .= "--------\n";
                $response .= "To learn more about the group, add one of the following words:\n";
                $response .= "-membership : group's membership details\n";
                $response .= "-settings : group's settings details\n";
        

                $bot->reply($response);
            }
        }, "Sorry, couldn't find a group with that email adddress");
    }

    

    public function showGroupMembership($bot, $email, $address)
    {
        return $this->handle($bot, function () use ($bot, $email, $address) {
            $url = config('services.ccps-gst.endpoint') . '/groups/' . $email . '/membership/'. $address . '/settings';
            $result = $this->helper->get($url)->getBody()->getContents();
            $theMember = json_decode($result)->member;
            
            $response = "*$address* is a member of group *email*\n";

            $response .= "---------\n";
            $response .= "Email: ". $theMember->email ."\n";
            $response .= "Type: ". $theMember->type ."\n";
            $response .= "Role: ". $theMember->role ."\n";
            $response .= "Status: ". $theMember->status ."\n";
            $response .= "Delivery Settings: ". $theMember->deliverySettings ."\n";
                

            $bot->reply($response);
        }, "Sorry, couldn't find a member of this group with that email address!");
    }
  
    public function showUserAccount($bot, $username, $config = null)
    {
        return $this->handle($bot, function () use ($bot, $username, $config) {
            $url = config('services.ccps-gst.endpoint') . '/users/' . $username . '/'. $config;
            $result = $this->helper->get($url)->getBody()->getContents();
            if ($config == "aliases") {
                $aliases = json_decode($result)->aliases;

                if (count($aliases) === 0) {
                    $bot->reply("User *$username* has no active aliases.");
                } else {
                    $aliasNames = collect($aliases)->pluck('alias')->implode(', ');
                    $bot->reply("User *$username* has the following aliases: " . $aliasNames);
                }
            } elseif ($config == "tokens") {
                $tokens = json_decode($result)->tokens;
                if (count($tokens) === 0) {
                    $bot->reply("User *$username* has no active tokens.");
                } else {
                    $tokenNames = collect($tokens)->pluck('displayText')->implode(', ');
                    $bot->reply("User *$username* has the following tokens: " . $tokenNames);
                }
            } elseif ($config == null) {
                $user = json_decode($result)->user;
                $OU = $user->orgUnitPath;
                $suspended = ($user->suspended) ? 'YES' : 'NO';
                $isAdmin = ($user->isAdmin) ? 'YES' : 'NO';

                $created = date('M j, Y, g:i a', strtotime($user->creationTime));
                $lastlogin = date('M j, Y, g:i a', strtotime($user->lastLoginTime));

                $aliases = json_decode($result)->user->aliases;

                if (!isset($aliases)) {
                    $aliasesResponse = "No listed aliases.";
                } else {
                    $aliasesList = collect($aliases)->map(function ($aliases) {
                        return '- ' . $aliases ;
                    })->implode("\n");
                    $aliasesResponse = "User *$username* has the following aliases:\n" . $aliasesList;
                }

                $organization = json_decode($result)->user->organizations;

                if (!isset($organization)) {
                    $organizationResponse = "No listed organizations.";
                } else {
                    $organizationList = collect($organization)->map(function ($organization) {
                        return '- Department: ' . $organization->department ;
                    })->implode("\n");
                    $organizationResponse = "User *$username* has the following organizations:\n" . $organizationList;
                }

                $relations = json_decode($result)->user->relations;

                if (!isset($relations)) {
                    $relationsResponse = "No listed relations.";
                } else {
                    $relationsList = collect($relations)->map(function ($relations) {
                        return '- ' . $relations->type . ': ' . $relations->value ;
                    })->implode("\n");
                    $relationsResponse = "User *$username* has the following relations:\n" . $relationsList;
                }

                $response = "User *$username:*\n";
                $response .= "- Name: {$user->name->fullName}\n";
                $response .= "- ID: {$user->id}\n";
                $response .= "- Is Admin? $isAdmin\n";

                $response .= "- Created at: $created\n";
                $response .= "- Last Login:  $lastlogin\n";
                $response .= "- Is Suspended? $suspended\n";
                $response .= "- OU: $OU\n";
                $response .= "- $aliasesResponse\n";
                $response .= "- $organizationResponse\n";
                $response .= "- $relationsResponse";

                $bot->reply($response);
            }
        }, "Sorry, couldn't find a user with that username!");
    }

    public function showUserEmail($bot, $username, $config = null)
    {
        return $this->handle($bot, function () use ($bot, $username, $config) {
            $url = config('services.ccps-gst.endpoint') . '/users/' . $username . '/email/'. $config;
            $result = $this->helper->get($url)->getBody()->getContents();
            $response = "";
            if ($config == "delegates") {
                $delegates = json_decode($result)->delegates;
                if (empty((array) $delegates)) {
                    $response .= "User *$username* has no email delegates.";
                } else {
                    $delegatesList = collect($delegates->delegates)->map(function ($delegates) {
                        return '- ' . $delegates->delegateEmail . ' (' . $delegates->verificationStatus . ')';
                    })->implode("\n");
                    $response .= "User *$username* has the following delegates:\n" . $delegatesList;
                }
            } elseif ($config == "filters") {
                $filters = json_decode($result)->filters->filter;

                if (count($filters) === 0) {
                    $response .= "User *$username* has no active filters.";
                } else {
                    // TODO: need to chunk this
    
                    $filtersList = collect($filters)->map(function ($filter) {
                        $criteria = implode('; ', array_filter((array) $filter->criteria));
                        $action = json_encode($filter->action);
                        return '- *Criteria*: ' . $criteria . ' => *Action*: ' . $action;
                    })->implode("\n");
                    $response .= "User *$username* has the following filters: " . $filtersList;
                }
            } elseif ($config == "forwards") {
                $forwards = json_decode($result)->forwards;
                if (empty((array) $forwards)) {
                    $response .= "User *$username* has no active forwards.";
                } else {
                    $forwardsList = collect($forwards->forwardingAddresses)->map(function ($forward) {
                        return '- ' . $forward->forwardingEmail . ' (' . $forward->verificationStatus . ')';
                    })->implode("\n");
                    $response .= "User *$username* has the following forwards:\n" . $forwardsList;
                }
            } elseif ($config == "imap") {
                $imap = json_decode($result)->imap;
                $response = "User *$username* has the following IMAP settings:\n";
                $response .= "- Enabled: " . ($imap->enabled == 1 ? 'Yes' : 'No') ."\n";
                $response .= "- Auto Expunge: ". ($imap->autoExpunge == 1 ? 'Yes' : 'No') ."\n";
                $response .= "- Expunge Behavior: " . $imap->expungeBehavior ."\n";

                if ($imap->expungeBehavior == "archive") {
                    $response .= "(Archive messages marked as deleted)\n";
                } elseif ($imap->expungeBehavior == "trash") {
                    $response .= "(Move messages marked as deleted to the trash.)\n";
                } elseif ($imap->expungeBehavior == "deleteforever") {
                    $response .= "(Immediately and permanently delete messages marked as deleted. The expunged messages cannot be recovered.)\n";
                }
            } elseif ($config == "labels") {
                $labels = json_decode($result)->labels;
                if (empty((array) $labels)) {
                    $bot->reply("User *$username* has no labels.");
                } else {
                    $response = "In addition to the 'System' labels, the user has the following labels:\n";
                    $labelList = collect($labels->labels)->map(function ($labels) {
                        if ($labels->type != "system") {
                            return '- ' . $labels->name . ' (' . $labels->id . ')';
                        }
                    })->implode("\n");
                    $response .= $labelList;
                }
            } elseif ($config == "pop") {
                $pop = json_decode($result)->pop;
                $response = "User *$username* has the following POP settings:\n";
                $response .= "- Access Window: " . $pop->accessWindow ."\n";
                if ($pop->accessWindow == "disabled") {
                    $response .= "(no messages are accessible via POP)\n";
                } elseif ($pop->accessWindow == "allMail") {
                    $response .= "(all unfetched messages are accessible via POP)\n";
                }
                $response .= "- Disposition: ". $pop->disposition ."\n";
            } elseif ($config == "sendas") {
                $sendAs = json_decode($result)->sendAs->sendAs;
                if (count($sendAs) === 1) {
                    $response = "User *$username* has standard SendAs configuration.\n";
                } else {
                    $response = "In addition to the standard SendAs configuration, user *$username* has the following SendAs configurations:\n";

                    $sendAsList = collect($sendAs)->map(function ($sendAs) {
                        if ($sendAs->isPrimary != true) {
                            return "- Display Name: " . $sendAs->displayName . "\n- Reply To Address: " . $sendAs->replyToAddress . "\n- Send As Email: " . $sendAs->sendAsEmail . "\n- Treat As Alas:" . ($sendAs->treatAsAlias == 1 ? 'Yes' : 'No') . "\n- Verfication Status: " . $sendAs->verificationStatus . "\n";
                        }
                    })->implode("-----\n");
                    $response .= $sendAsList;
                    //$bot->reply("User *$username* has the following SendAs configuration:\n" . $sendAsList);
                }
            } elseif ($config == "vacation") {
                $vacation = json_decode($result)->vacation;
                $response = "User *$username* has the following Out Of Office/Vacation settings:\n";
                $response .= "- Enabled Auto Reply: " . ($vacation->enableAutoReply == 1 ? 'Yes' : 'No') ."\n";
                $response .= "- Restrict to Contacts: ". ($vacation->restrictToContacts == 1 ? 'Yes' : 'No') ."\n";
                $response .= "- Restrict to Domain: " . ($vacation->restrictToDomain == 1 ? 'Yes' : 'No') ."\n";
                $response .= "- Start: " . (gmdate('M j, Y', floor($vacation->startTime / 1000))) ."\n";
                $response .= "- End: " . (gmdate('M j, Y', floor($vacation->endTime / 1000))) ."\n";
                $response .= "- Vacation Message:\n----------------\n";

                if ($vacation->responseSubject != null) {
                    $response .= "SUBJECT: $vacation->responseSubject\n";
                }
                if ($vacation->responseBodyPlainText != null) {
                    $response .= "Message (Plain Text): $vacation->responseBodyPlainText\n";
                }
                if ($vacation->responseBodyHtml != null) {
                    $response .= "MESSAGE (HTML): $vacation->responseBodyHtml\n";
                }
            } else {
                $profile = json_decode($result)->profile;
                $response = "User *$username* has " . $profile->messagesTotal ." messages (". $profile->threadsTotal ." threads).\n";
                $response .= "--------\n";
                $response .= "To learn more about the user's email configuration, add one of the following words:\n";
                $response .= "delegates : user's email delegation\n";
                $response .= "filters : user's email filters\n";
                $response .= "fowards : user's email forwards\n";
                $response .= "imap : user's email IMAP settings\n";
                $response .= "labels : user's email labels\n";
                $response .= "pop : user's email POP settings\n";
                $response .= "sendas : user's email SendAs settings\n";
                $response .= "vacation : user's email Out of Office\Vacation settings\n";
            }
            $bot->reply($response);
        }, "Sorry, couldn't find a user with that username!");
    }

    public function showUserGroups($bot, $username)
    {
        return $this->handle($bot, function () use ($bot, $username) {
            $url = config('services.ccps-gst.endpoint') . '/users/' . $username . '/groups';
            $result = $this->helper->get($url)->getBody()->getContents();

            $groups = json_decode($result)->groups;

            if (count($groups) === 0) {
                $bot->reply("User *$username* has no active groups.");
            } else {
                $groupChunks = collect($groups)->pluck('email')->chunk(50);

                $bot->userStorage()->save([
                    'chunks' => [
                        'username'      => $username,
                        'type'          => 'group',
                        'chunks'        => $groupChunks,
                        'previousChunk' => 0,
                        'lastChunk'     => $groupChunks->count() - 1
                    ]
                ]);

                $this->showGroupChunk(0, $bot);
            }
        }, "Sorry, couldn't find a user with that username!");
    }

    public function showWhatis($bot, $query)
    {
        return $this->handle($bot, function () use ($bot, $query) {
            $url = config('services.ccps-gst.endpoint') . '/whatis/' . $query;
            $result = $this->helper->get($url)->getBody()->getContents();
            
            
            // Fixing JSON Decode issue re: UNICODE NULL
            $result = str_replace('\u0000', '', $result);
            \Log::debug($result);

            $thing = json_decode($result)->thing;

            $response = 'you said *' . $query . '* which is ' . $thing->prefix .' ' . $thing->type;
            if ($thing->type == 'alias') {
                $response .= " (of " . $thing->primaryEmail . ")";
            }

            $bot->reply($response);
        }, "Sorry, couldn't find an entity with that username!");

        // $bot->reply('you said ' . $query);
    }

    public function showGroupChunk($index, $bot)
    {
        return $this->handle($bot, function () use ($index, $bot) {
            $chunks = $bot->userStorage()->get('chunks');
            if ($index > $chunks['lastChunk']) {
                $bot->reply('No more chunks to show.');
            } elseif ($chunks['type'] !== 'group') {
                $bot->reply('Error: invalid group chunks.');
            } else {
                $groupInfo = "";
                foreach ($chunks['chunks'][$index] as $group) {
                    $groupInfo .= "- $group\n";
                }
                $indexDisplay = $index + 1;
                $lastChunkDisplay = $chunks['lastChunk'] + 1;

                $bot->reply("Groups (chunk $indexDisplay of {$lastChunkDisplay}) for user *{$chunks['username']}*:\n" . $groupInfo);

                $chunks['previousChunk'] = $index;
                $bot->userStorage()->save(['chunks' => $chunks]);
            }
        });
    }
}
