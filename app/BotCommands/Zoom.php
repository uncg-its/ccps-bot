<?php

namespace App\BotCommands;

use Carbon\Carbon;
use App\Helpers\Zst;

class Zoom extends Base
{
    protected $helper;
    protected $service = 'zoom';

    public function __construct()
    {
        $this->helper = new Zst;
    }

    public function help($bot)
    {
        return $this->handle($bot, function () use ($bot) {
            $bot->reply("Zoom commands:\n\n*zoom user {username} account* - looks up a user's basic information and settings, including licensing\n*zoom user {username} meetings* - shows scheduled meetings for a user (chunked)\n*zoom user {username} recordings* - lists recent recordings for a user (default past month, chunked)\n");
        });
    }

    public function searchUser($bot, $prefix, $username)
    {
        return $this->handle($bot, function () use ($bot, $username) {
            //user
            $url = config('services.ccps-zst.endpoint') . '/users/' . $username;
            $result = $this->helper->get($url)->getBody()->getContents();

            $user = json_decode($result)->user;

            $response = "User *$username*:\n- Zoom ID: {$user->id}\n- Name: {$user->first_name} {$user->last_name}\n- Status: {$user->status}\n- Created at: {$user->created_at}\n- Last Login: {$user->last_login_time}\n";
            
            // If no record of a desktop client exists, the endpoint currently does not return a last_client_version.
            if (isset($user->last_client_version)) {
                $response .= "- Last Client Version: {$user->last_client_version}\n";
            } else {
                $response .= "- Last Client Version: No record of desktop client usage.\n";
            }


            // settings
            $url = config('services.ccps-zst.endpoint') . '/users/' . $username . '/settings';
            $result = $this->helper->get($url)->getBody()->getContents();

            $settings = json_decode($result)->settings;
            $webinarEnabled = $settings->feature->webinar ? 'YES' : 'NO';

            $response .= "- Meeting Capacity: {$settings->feature->meeting_capacity}\n- Webinar enabled?: {$webinarEnabled}";

            $bot->reply($response);
        }, "Sorry, I couldn't find a user with that username!");
    }

    public function userMeetings($bot, $username)
    {
        return $this->handle($bot, function () use ($bot, $username) {
            // TODO: custom date range, chunking
            $url = config('services.ccps-zst.endpoint') . '/users/' . $username . '/meetings';
            $result = $this->helper->get($url)->getBody()->getContents();

            $meetings = json_decode($result)->meetings;

            $response = "Meetings for *$username*:\n";
            foreach ($meetings as $meeting) {
                $response .= "- {$meeting->topic} (ID: {$meeting->id}) - starts: {$meeting->start_time}\n";
            }

            $bot->reply($response);
        }, "Sorry, I couldn't find a user with that username!");
    }

    public function userRecordings($bot, $username)
    {
        return $this->handle($bot, function () use ($bot, $username) {
            // TODO: custom date range, chunking
            $url = config('services.ccps-zst.endpoint') . '/users/' . $username . '/recordings';
            $result = $this->helper->get($url)->getBody()->getContents();

            $recordings = json_decode($result)->recordings;

            $response = "Recordings for *$username*:\n";
            foreach ($recordings as $recording) {
                $response .= "- {$recording->topic} (ID: {$recording->id}) - created: {$recording->start_time} - duration: {$recording->duration}min\n";
            }

            $bot->reply($response);
        }, "Sorry, I couldn't find a user with that username!");
    }



    // public function listUserCourses($bot, $prefix, $username, $term = null)
    // {
    //     $suffix = is_null($term) ? '' : '?term=' . $term;

    //     return $this->handle($bot, function () use ($bot, $username, $term, $suffix) {
    //         $url = config('services.ccps-zst.endpoint') . '/users/' . $username . '/courses' . $suffix;
    //         $result = $this->helper->get($url)->getBody()->getContents();

    //         $courses = json_decode($result)->courses;
    //         $term = json_decode($result)->term;

    //         if (count($courses) === 0) {
    //             $bot->reply("User *$username* has no courses term *$term*.");
    //         } else {
    //             $courseInfo = "";
    //             foreach ($courses as $course) {
    //                 $courseInfo .= "- [{$course->course_code}] {$course->name} ({$course->workflow_state}) - _Canvas ID {$course->id}_\n";
    //             }
    //             $bot->reply("User *$username* is enrolled in the following courses for term *$term*:\n" . $courseInfo);
    //         }
    //     }, "Sorry, I couldn't find a user with that username!");
    // }

    // public function listCourseEnrollments($bot, $id)
    // {
    //     return $this->handle($bot, function () use ($bot, $id) {
    //         $url = config('services.ccps-zst.endpoint') . '/courses/' . $id . '/enrollments';
    //         $result = $this->helper->get($url)->getBody()->getContents();

    //         $enrollments = json_decode($result)->enrollments;

    //         if (count($enrollments) === 0) {
    //             $bot->reply("Course *$id* has no enrollments.");
    //         } else {
    //             // Google Chat driver can't reply more than once, so this is necessary.
    //             $enrollmentChunks = collect($enrollments)->chunk(50);

    //             $bot->userStorage()->save([
    //                 'chunks' => [
    //                     'id'            => $id,
    //                     'type'          => 'enrollment',
    //                     'chunks'        => $enrollmentChunks,
    //                     'previousChunk' => 0,
    //                     'lastChunk'     => $enrollmentChunks->count() - 1
    //                 ]
    //             ]);

    //             $this->showEnrollmentChunk(0, $bot);
    //         }
    //     }, "Sorry, I couldn't find a course with that ID!");
    // }

    // public function showCourse($bot, $prefix, $id)
    // {
    //     return $this->handle($bot, function () use ($bot, $id) {
    //         $url = config('services.ccps-zst.endpoint') . '/courses/' . $id;
    //         $result = $this->helper->get($url)->getBody()->getContents();

    //         $course = json_decode($result)->course;
    //         $teachers = collect($course->teachers)->implode('display_name', ', ');
    //         $concluded = $course->concluded ? 'yes' : 'no';
    //         $start = $course->start_at ?? ($course->term->start_at ?? 'not set');
    //         if ($start !== 'not set') {
    //             $start = Carbon::parse($start)->setTimezone('America/New_York')->format('m/d/Y H:i:s T');
    //         }
    //         $end = $course->end_at ?? ($course->term->end_at ?? 'not set');
    //         if ($end !== 'not set') {
    //             $end = Carbon::parse($end)->setTimezone('America/New_York')->format('m/d/Y H:i:s T');
    //         }
    //         $restricted = $course->restrict_enrollments_to_course_dates ? 'yes' : 'no';

    //         $response = "Course *$id:*\n- Name: {$course->name}\n- Code: {$course->course_code}\n- Term: {$course->term->name}\n- Total Students: {$course->total_students}\n- Workflow state: {$course->workflow_state}\n- Teacher(s): {$teachers}\n- Start Date: {$start}\n- End Date: {$end}\n- Access Restricted to Course Dates?: {$restricted}\n- Concluded?: {$concluded}\n- Created at: {$course->created_at}";
    //         $bot->reply($response);
    //     }, "Sorry, couldn't find a course with that ID! Make sure you're using the Canvas ID, which is a number.");
    // }

    // public function showEnrollmentChunk($index, $bot)
    // {
    //     return $this->handle($bot, function () use ($index, $bot) {
    //         $chunks = $bot->userStorage()->get('chunks');
    //         if ($index > $chunks['lastChunk']) {
    //             $bot->reply('No more chunks to show.');
    //         } elseif ($chunks['type'] !== 'enrollment') {
    //             $bot->reply('Error: invalid enrollment chunks.');
    //         } else {
    //             $enrollmentInfo = "";
    //             foreach ($chunks['chunks'][$index] as $enrollment) {
    //                 $enrollment = json_decode(json_encode($enrollment));
    //                 $enrollmentInfo .= "- {$enrollment->user->sortable_name} ({$enrollment->user->login_id}): _{$enrollment->role}_\n";
    //             }
    //             $indexDisplay = $index + 1;
    //             $lastChunkDisplay = $chunks['lastChunk'] + 1;

    //             $bot->reply("Enrollments (chunk $indexDisplay of {$lastChunkDisplay}) for course *{$chunks['id']}*:\n" . $enrollmentInfo);

    //             $chunks['previousChunk'] = $index;
    //             $bot->userStorage()->save(['chunks' => $chunks]);
    //         }
    //     });
    // }
}
