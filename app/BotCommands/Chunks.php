<?php

namespace App\BotCommands;

class Chunks
{
    protected $chunkMap = [
        'enrollment' => 'Canvas@showEnrollmentChunk',
        'group'      => 'Google@showGroupChunk',
    ];

    public function showNextChunk($bot)
    {
        $chunks = $bot->userStorage()->get('chunks');
        if (empty($chunks)) {
            $bot->reply('No chunks to show!');
            return;
        }

        $nextChunk = $chunks['previousChunk'] + 1;
        $type = $chunks['type'];

        if (!isset($this->chunkMap[$type])) {
            throw new \OutOfRangeException('No configuration for chunk type ' . $type . ' exists');
        }

        list($commandClass, $method) = explode('@', $this->chunkMap[$type]);

        $className = '\\App\\BotCommands\\' . $commandClass;

        $class = new $className;
        $class->$method($nextChunk, $bot);
    }
}
