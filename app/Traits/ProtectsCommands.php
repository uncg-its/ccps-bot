<?php

namespace App\Traits;

trait ProtectsCommands
{
    protected $adminUsers = [
        'mslibera@uncg.edu',
    ];

    protected function userIsAdmin($bot)
    {
        if (!isset($bot->getUser()->getInfo()['email'])) {
            return false;
        }
        $email = $bot->getUser()->getInfo()['email'];
        return in_array($email, $this->adminUsers);
    }
}
