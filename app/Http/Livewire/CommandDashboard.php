<?php

namespace App\Http\Livewire;

use App\Command;
use Livewire\Component;

class CommandDashboard extends Component
{
    public $recent = 5;
    public $recentUnderstood;
    public $recentNotUnderstood;
    public $byService;
    public $byCommand;
    public $activeUsers;

    public $recentOptions = [
        3  => 3,
        5  => 5,
        10 => 10
    ];

    public function mount()
    {
        $this->loadStats();
    }

    public function render()
    {
        return view('livewire.command-dashboard')
            ->layout('layouts.wrapper', [
                'pageTitle' => 'Command Dashboard'
            ]);
    }

    public function loadStats()
    {
        $this->recentUnderstood = Command::recent($this->recent)->understood()->get();
        $this->recentNotUnderstood = Command::recent($this->recent)->notUnderstood()->get();
        $this->byService = Command::select('service', \DB::raw('COUNT(id) as count'))
            ->understood()
            ->groupBy('service')
            ->orderByDesc('count')
            ->limit($this->recent)
            ->get();
        $this->byCommand = Command::select(\DB::raw('CONCAT(service, ".", core_command) as command'), \DB::raw('COUNT(id) as count'))
            ->understood()
            ->groupBy('command')
            ->orderByDesc('count')
            ->limit($this->recent)
            ->get();
        $this->activeUsers = Command::select('email', \DB::raw('COUNT(id) as count'))
            ->groupBy('email')
            ->orderByDesc('count')
            ->limit($this->recent)
            ->get();
    }

    public function updatedRecent()
    {
        $this->loadStats();
    }
}
