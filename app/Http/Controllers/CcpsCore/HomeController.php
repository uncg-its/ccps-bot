<?php

namespace App\Http\Controllers\CcpsCore;

use Uncgits\Ccps\Controllers\HomeController as BaseController;

class HomeController extends BaseController
{
    public function __construct() {
        parent::__construct();
    }
}
