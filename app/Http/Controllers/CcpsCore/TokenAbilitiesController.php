<?php

namespace App\Http\Controllers\CcpsCore;

use Uncgits\Ccps\Controllers\TokenAbilitiesController as BaseController;

class TokenAbilitiesController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }
}
