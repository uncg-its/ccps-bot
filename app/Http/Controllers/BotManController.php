<?php

namespace App\Http\Controllers;

use BotMan\BotMan\BotMan;
use App\Conversations\ExampleConversation;
use App\Http\Middleware\LogMessage;

class BotManController extends Controller
{
    public function handle()
    {
        // init
        $botman = app('botman');

        // set prefix for some chat clients
        $prefix = '(@CCPSBot )?';

        // Workspace AKA G Suite
        $googleKeywords = '(workspace|gsuite|google)';

        // WHATIS (workspace & gsuite)
        $botman->hears($prefix . $googleKeywords . ' whatis {query}', '\App\BotCommands\Google@showWhatis');

        // GROUP (workspace & gsuite)
        $botman->hears($prefix . $googleKeywords . ' group {email} info {config}', '\App\BotCommands\Google@showGroup');
        $botman->hears($prefix . $googleKeywords . ' group {email} info', '\App\BotCommands\Google@showGroup');
        $botman->hears($prefix . $googleKeywords . ' group {email} member {address}', '\App\BotCommands\Google@showGroupMembership');

        // USER (workspace & gsuite)
        $botman->hears($prefix . $googleKeywords . ' user {username} account {config}', '\App\BotCommands\Google@showUserAccount');
        $botman->hears($prefix . $googleKeywords . ' user {username} account', '\App\BotCommands\Google@showUserAccount');
        $botman->hears($prefix . $googleKeywords . ' user {username} email {config}', '\App\BotCommands\Google@showUserEmail');
        $botman->hears($prefix . $googleKeywords . ' user {username} email', '\App\BotCommands\Google@showUserEmail');
        $botman->hears($prefix . $googleKeywords . ' user {username} groups', '\App\BotCommands\Google@showUserGroups');

        // Canvas
        $botman->hears($prefix . 'canvas user ([A-Za-z0-9_\-"]*) quota', '\App\BotCommands\Canvas@showUserQuota');
        $botman->hears($prefix . 'canvas user ([A-Za-z0-9_\-"]*)$', '\App\BotCommands\Canvas@searchUser');
        $botman->hears($prefix . 'canvas courses ([A-Za-z0-9_\-"]*) term ([0-9]*)', '\App\BotCommands\Canvas@listUserCourses');
        $botman->hears($prefix . 'canvas courses ([A-Za-z0-9_\-]*)$', '\App\BotCommands\Canvas@listUserCourses');
        $botman->hears($prefix . 'canvas findcourse ([A-Za-z0-9_\-"]*) term ([0-9]*)', '\App\BotCommands\Canvas@searchCourses');
        $botman->hears($prefix . 'canvas findcourse ([A-Za-z0-9-_]*)$', '\App\BotCommands\Canvas@searchCourses');
        $botman->hears($prefix . 'canvas findcourse ("[A-Za-z0-9-_ ]*")$', '\App\BotCommands\Canvas@searchCourses');
        $botman->hears($prefix . 'canvas course ([0-9]*)$', '\App\BotCommands\Canvas@showCourse');
        $botman->hears($prefix . 'canvas course {id} enrollments', '\App\BotCommands\Canvas@listCourseEnrollments');
        $botman->hears($prefix . 'canvas course {id} quota', '\App\BotCommands\Canvas@showCourseQuota');

        // Zoom
        $botman->hears($prefix . 'zoom user ([A-Za-z0-9_\-]*)( account)?$', '\App\BotCommands\Zoom@searchUser');
        $botman->hears($prefix . 'zoom user {username} meetings', '\App\BotCommands\Zoom@userMeetings');
        $botman->hears($prefix . 'zoom user {username} recordings', '\App\BotCommands\Zoom@userRecordings');

        // LDAP
        $botman->hears($prefix . 'ldap user {username}', 'App\BotCommands\Ldap@lookupUser');

        // Grouper
        $botman->hears($prefix . 'grouper user {username}', 'App\BotCommands\Grouper@lookupUser');

        // Chunking
        $botman->hears($prefix . 'next', '\App\BotCommands\Chunks@showNextChunk');
        $botman->hears($prefix . 'next chunk', '\App\BotCommands\Chunks@showNextChunk');
        $botman->hears($prefix . 'next page', '\App\BotCommands\Chunks@showNextChunk');

        // Help
        $botman->hears($prefix . '(help(\?)?|what can you do(\?)?)+', '\App\BotCommands\General@help');
        $botman->hears($prefix . 'help ' . $googleKeywords, '\App\BotCommands\Google@help');
        $botman->hears($prefix . 'help canvas', '\App\BotCommands\Canvas@help');
        $botman->hears($prefix . 'help zoom', '\App\BotCommands\Zoom@help');
        $botman->hears($prefix . 'help ldap', '\App\BotCommands\Ldap@help');
        $botman->hears($prefix . 'help grouper', '\App\BotCommands\Grouper@help');

        // General
        $botman->hears($prefix . '(hi|hello|hola|guten tag|ciao)', '\App\BotCommands\General@hello');
        $botman->hears($prefix . 'ccps', '\App\BotCommands\General@ccps');

        // Debug and testing
        $botman->hears($prefix . 'debug me', '\App\BotCommands\Debug@me');
        $botman->hears($prefix . 'debug payload', '\App\BotCommands\Debug@payload');
        $botman->hears($prefix . 'debug driver', '\App\BotCommands\Debug@driver');

        $botman->hears($prefix . 'protected', '\App\BotCommands\Debug@protected');

        // Fallback
        $botman->fallback('\App\BotCommands\General@fallback');

        // logging and listening
        $botman->middleware->sending(new LogMessage);
        $botman->listen();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tinker()
    {
        $endpoint = config('ccps-bot.url_suffix');
        $prefix = env('APP_SUBFOLDER_PREFIX', '');
        if ($prefix != '') {
            $endpoint = $prefix . '/' . $endpoint;
        }


        return view('tinker')->with(['endpoint' => $endpoint]);
    }

    /**
     * Loaded through routes/botman.php
     *
     * @param BotMan $bot
     */
    public function startConversation(BotMan $bot)
    {
        $bot->startConversation(new ExampleConversation());
    }
}
