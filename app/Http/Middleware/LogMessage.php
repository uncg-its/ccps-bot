<?php

namespace App\Http\Middleware;

use App\Command;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Interfaces\Middleware\Sending;

class LogMessage implements Sending
{
    /**
     * Handle an outgoing message payload before/after it
     * hits the message service.
     *
     * @param mixed $payload
     * @param callable $next
     * @param BotMan $bot
     *
     * @return mixed
     */
    public function sending($payload, $next, BotMan $bot)
    {
        if (!is_null($payload)) {
            $message = isset($payload['text']) ? $payload['text'] : $payload['message']->getText();

            Command::logFromBot($bot, strpos($message, "I don't understand") === false);
            return $next($payload);
        }
    }
}
