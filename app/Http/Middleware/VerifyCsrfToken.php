<?php

namespace App\Http\Middleware;

use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [];

    public function __construct(Application $app, Encrypter $encrypter)
    {
        $prefix = env('APP_SUBFOLDER_PREFIX', '');
        if ($prefix != '') {
            $this->except[] = env('APP_SUBFOLDER_PREFIX') . '/' . env('BOTMAN_URL_SUFFIX', 'botman');
        } else {
            $this->except[] = env('BOTMAN_URL_SUFFIX', 'botman');
        }
        parent::__construct($app, $encrypter);
    }
}
