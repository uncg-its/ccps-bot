<?php

namespace App\Http\Middleware;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\Interfaces\Middleware\Received;
use BotMan\BotMan\Messages\Incoming\IncomingMessage;

class DumpReceived implements Received
{
    public function received(IncomingMessage $message, $next, BotMan $bot)
    {
        // dump($message, $message->getConversationIdentifier(), $message->getOriginatedConversationIdentifier());
        dump($message, $message->getSender());
        return $next($message);
    }
}
