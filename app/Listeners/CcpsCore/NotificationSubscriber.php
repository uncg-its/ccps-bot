<?php

namespace App\Listeners\CcpsCore;

use App\Events\CcpsCore\StaleLockFileDetected;
use Uncgits\Ccps\Listeners\NotificationSubscriber as BaseListener;

class NotificationSubscriber extends BaseListener
{
    public function subscribe($events)
    {
        // Remove this if you do not want notifications for the Stale Lock File job.
        $events->listen(
            StaleLockFileDetected::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );

        // $events->listen(
        //     'My\Event\Class',
        //     'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        // );

        parent::subscribe($events);
    }
}
