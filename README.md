# CCPS Bot

More documentation coming someday.

# Version History

## 0.5.1

- Fix bug with chunks stemming from Google command class rename in 0.2
- Alias "page" for "chunk"

## 0.5

- User Lookup functionality

## 0.4

- CCPS Core 3.3

## 0.3.1

- logging to splunk

## 0.3

- CCPS Core 3.2.*
- Laravel 8.0+
- PHP 8 readiness
- intermediary (hopefully) packages for botman and studio, compatible with PHP 8

## 0.2.4

- Bugfix for new Google prefixes
- Zoom streamlining using regex

## 0.2.3

- easter egg escaping

## 0.2.2

- easter egg monospacing

## 0.2.1

- composer updates

## 0.2

- renames GSuite to Google and provides alternative keywords for accessing those services
- additional features around email and groups (Google)

## 0.1.1

- Adds Google Group queries

## 0.1

- First official versioning. Supports basic functionality across: Canvas, G Suite, Zoom
