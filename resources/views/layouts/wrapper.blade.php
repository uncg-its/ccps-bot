<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} | {{ $pageTitle }}</title>

    <!-- Styles -->
    @livewireStyles
    <link rel="stylesheet" href="{{ asset('css/ccps-core.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Page head items -->
    @stack('head')
</head>
<body class="relative">
@include('components.nav')
<div class="container max-w-full px-4 sm:px-6 lg:px-8">
    <livewire:flash-container />
    @yield('content')
    {!! $slot ?? '' !!}
</div>

<!-- Scripts -->
<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>

@livewireScripts
<script src="https://kit.fontawesome.com/673452eb06.js" crossorigin="anonymous"></script>
<script src="{{ asset('js/ccps-core.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>

<!-- Page scripts -->
@stack('scripts')
</body>
</html>
