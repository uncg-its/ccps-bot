<div wire:poll.10s="loadStats">
    <x-h1>Command Dashboard</x-h1>
    <x-p>Here you will be able to see the commands that have been run, and look at statistics to better improve the bot.</x-p>
    <div class="flex my-3">
        <x-input-select wire:model="recent" name="Number to Show" :options="$recentOptions"/>
    </div>
    <div wire:loading wire:target="recent" class="w-full">
        <x-alert-info :dismissable="false" icon="spinner fa-spin">
            Loading...
        </x-alert-info>
    </div>
    <div class="flex gap-4" wire:loading.remove wire:target="recent">
        <div class="w-1/2">
            <x-card title="Last {{ $recent }} understood commands">
                @foreach ($recentUnderstood as $command)
                    <x-p>
                        <x-code>{{ $command['message'] }}</x-code>
                    </x-p>
                @endforeach
            </x-card>
            <x-card title="Last {{ $recent }} not-understood commands">
                @foreach ($recentNotUnderstood as $command)
                    <x-p>
                        <x-code>{{ $command['message'] }}</x-code>
                    </x-p>
                @endforeach
            </x-card>
        </div>
        <div class="w-1/2">
            <x-card title="Top {{ $recent }} Services queried">
                @foreach ($byService as $service)
                    <x-p><strong>{{ $service->service ?? 'none' }}</strong>: {{ $service->count }}</x-p>
                @endforeach
            </x-card>
            <x-card title="Top {{ $recent }} Commands issued">
                @foreach ($byCommand as $command)
                    <x-p><strong>{{ $command->command ?? 'none' }}</strong>: {{ $command->count }}</x-p>
                @endforeach
            </x-card>
            <x-card title="Top {{ $recent }} active users">
                @foreach ($activeUsers as $activeUser)
                    <x-p><strong>{{ $activeUser->email ?? 'unknowon' }}</strong>: {{ $activeUser->count }}</x-p>
                @endforeach
            </x-card>
        </div>
    </div>

    {{-- <div id="chart" class="w-full h-64"></div> --}}

    {{-- @push('scripts')
        <!-- Charting library -->
        <script src="https://unpkg.com/chart.js@2.9.3/dist/Chart.min.js"></script>
        <!-- Chartisan -->
        <script src="https://unpkg.com/@chartisan/chartjs@^2.1.0/dist/chartisan_chartjs.umd.js"></script>

        <script>
            const chart = new Chartisan({
                el: '#chart',
                data: {
                    chart: { labels: ['First', 'Second', 'Third'] },
                    datasets: [
                        { name: 'Sample 1', values: [10, 3, 7]},
                        { name: 'Sample 2', values: [1, 6, 2]},
                    ]
                },
                hooks: new ChartisanHooks()
                    .colors(['#ECC94B', '#4299E1'])
                    .responsive()
                    .beginAtZero()
                    .legend({ position: 'bottom' })
                    .title('This is a sample chart using chartisan!')
                    .datasets([{ type: 'line', fill: false }, 'bar']),
            })
        </script>
    @endpush --}}
</div>
