<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model'  => App\CcpsCore\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'client_id'     => env('SOCIALITE_GOOGLE_CLIENT_ID'),
        'client_secret' => env('SOCIALITE_GOOGLE_CLIENT_SECRET'),
        'redirect'      => env('SOCIALITE_GOOGLE_REDIRECT'),
    ],

    'azure' => [
        'client_id'     => env('SOCIALITE_AZURE_KEY'),
        'client_secret' => env('SOCIALITE_AZURE_SECRET'),
        'redirect'      => env('SOCIALITE_AZURE_REDIRECT_URI'),
        'force_mfa' => env('SOCIALITE_AZURE_FORCE_MFA', false),
    ],

    'ccps-gst' => [
        'key'      => env('CCPS_GST_KEY'),
        'endpoint' => env('CCPS_GST_ENDPOINT'),
    ],

    'ccps-cst' => [
        'key'      => env('CCPS_CST_KEY'),
        'endpoint' => env('CCPS_CST_ENDPOINT'),
    ],

    'ccps-zst' => [
        'key'      => env('CCPS_ZST_KEY'),
        'endpoint' => env('CCPS_ZST_ENDPOINT'),
    ],

    'ccps-user-lookup' => [
        'key'      => env('CCPS_USER_LOOKUP_KEY'),
        'endpoint' => env('CCPS_USER_LOOKUP_ENDPOINT'),
    ]

];
