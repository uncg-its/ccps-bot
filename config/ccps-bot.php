<?php

return [
    'proxy' => [
        'enabled' => env('CCPSBOT_PROXY_ENABLED', false),
        'host'    => env('CCPSBOT_PROXY_HOST', ''),
        'port'    => env('CCPSBOT_PROXY_PORT', '')
    ],

    'url_suffix' => env('BOTMAN_URL_SUFFIX', 'botman'),
];
